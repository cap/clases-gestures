//
//  LongPressViewController.swift
//  clases-gestures
//
//  Created by Admin on 5/10/17.
//  Copyright © 2017 Luis Gonzales. All rights reserved.
//

import UIKit

class LongPressViewController: UIViewController {

    var estaCreciendo = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
    @IBOutlet weak var viewTravieso: UIView!

    
    @IBAction func onLongPress(_ sender: AnyObject) {
        viewTravieso.backgroundColor = .green 
        
    }
    @IBAction func onTap(_ sender: AnyObject) {
        crece()
    }
    
    func crece(){
        if !estaCreciendo {
            estaCreciendo = true
            
            UIView.animate(withDuration: 2, animations: {self.viewTravieso.transform.scaledBy(x: 2, y: 2)})
        }
    }
}
