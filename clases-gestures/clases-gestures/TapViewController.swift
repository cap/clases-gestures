//
//  TapViewController.swift
//  clases-gestures
//
//  Created by Admin on 5/10/17.
//  Copyright © 2017 Luis Gonzales. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    var primerColor = UIColor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        primerColor = self.view.backgroundColor!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func onTap(_ sender: AnyObject) {
     
        let segundoColor = UIColor.blue
        if self.view.backgroundColor == segundoColor{
            self.view.backgroundColor = primerColor
        }
        else{
            self.view.backgroundColor = segundoColor
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
