//
//  PinchViewController.swift
//  clases-gestures
//
//  Created by Admin on 5/10/17.
//  Copyright © 2017 Luis Gonzales. All rights reserved.
//

import UIKit

class PinchViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    

    @IBOutlet weak var viewTravieso: UIView!
    

    @IBAction func onPinch(_ sender: AnyObject) {
        
        if let pinchGR = sender as? UIPinchGestureRecognizer{
            let escale = pinchGR.scale
            viewTravieso.transform = viewTravieso.transform.scaledBy(x: escale, y: escale)
            
            //viewTravieso.transform = CGAffineTransform(scaleX: escale, y: escale)
            
            pinchGR.scale = 1
        }
    }
}
