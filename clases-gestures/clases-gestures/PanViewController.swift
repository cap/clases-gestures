//
//  PanViewController.swift
//  clases-gestures
//
//  Created by Admin on 5/10/17.
//  Copyright © 2017 Luis Gonzales. All rights reserved.
//

import UIKit

class PanViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet weak var viewTravieso: UIView!
   
    @IBAction func onPan(_ sender: UIPanGestureRecognizer) {
        let puntoDeToque = sender.location(in: self.view)
        viewTravieso.center = puntoDeToque
    }
}
